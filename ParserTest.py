# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 17:41:18 2020

Парсер 

@author: serge
"""

class ParserMFrac:
    def __init__(self):
        class RockProp:
            StartLine=0
            EndLine=None
            fnameTXT="./Files/RockProp.txt"
            fnameCSV="./Files/RockProp.csv"
            pattern="Свойства пород"
            flag=0
        self.fname2parse=None
        self.RP=RockProp()
        
    def fileopendialog(self): #данный диалог использует tkinter для выбора файла
        import tkinter as tk
        from tkinter import filedialog
    
        root = tk.Tk()
        root.wm_attributes('-topmost', 1) #при вызове диалогового окна нужно, чтобы было поверх всех окон
        root.withdraw()
        
        self.fname2parse = filedialog.askopenfile(parent=root,mode='r',filetypes=[("Файл с данными MFrac","*.txt")],title='Выберите файл с данными MFrac')
        
        if self.fname2parse != None:
            print ("Был выбран этот файл с данными MFrac: ", self.fname2parse)
        

    def readMFracfile(self):
        

        import codecs
        
        self.fileopendialog()
        
        count = 0
        with codecs.open(self.fname2parse.name,"r","utf_8_sig") as fp: 
            Lines = fp.readlines() 
            for line in Lines: 
                count += 1
                print("Line{}: {}".format(count, line.strip())) 
                
        # Writing to file 
        with open("myfile.txt", "w") as fp: 
            fp.writelines(Lines[0:10])
            
    def SectionSplitter(self):
        import codecs
        
        self.fileopendialog()
        
       
        with codecs.open(self.fname2parse.name,"r","utf_8_sig") as fp: 
            Lines = fp.read()
            
        Section=Lines.split("\r\n\r\n")
        
        print("Section[0]: ",Section[1])
        
    def CutSectionByPatt(self):
        
        import codecs
        import re
        
        self.fileopendialog()
        
        
        with codecs.open(self.fname2parse.name,"r","utf_8_sig") as fp:
            Lines = fp.readlines()
            for i, line in enumerate(Lines):
                
                if re.search(self.RP.pattern, line):
                    self.RP.StartLine=i
                    self.RP.flag=2
                    print('self.RP.StartLine = ',self.RP.StartLine)
                if (i>self.RP.StartLine)&(self.RP.flag==2):
                    if (line=="\r\n"):
                        self.RP.EndLine=i
                        print('self.RP.EndLine = ',self.RP.EndLine)
                        self.RP.flag=1
        with codecs.open(self.RP.fnameTXT, "w",encoding="utf_8_sig") as fp: 
            fp.writelines(Lines[self.RP.StartLine:self.RP.EndLine])
    
    def ReadCSV(self):
        
        import codecs
        
        import csv
        
        import pandas as pd
        
        import re
        
        import numpy as np
        
        with codecs.open(self.RP.fnameTXT, "r",encoding="utf_8_sig") as fp:
            
            
            
# =============================================================================
#             stripped=(line.strip() for line in fp)
#             
#             
#             
#             
#             
# 
#             
#             lines=(line.split() for line in stripped if line)
#             
#             
#             with codecs.open(self.RP.fnameCSV, 'w',encoding="utf_8_sig") as out_file:
#                 writer=csv.writer(out_file)
#                 writer.writerows(lines)
# =============================================================================
                
            Lines=fp.readlines()
            #print('Lines= ',Lines[24])
            
            L=np.empty([0,2])
            
            for i, line in enumerate(Lines):
               
                print('line = ',line)
                if (i>6):
                    
                    temp=re.compile(r'(\D+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)(\D+)')
                    res=temp.match(line).groups()
                    L=np.append(L,[res])
                     
            print('L = ',L)   
            
            #temp = re.compile("([a-zA-Z]+)([0-9]+)")
            
            #temp = re.compile("([0-9.E+]+)")
            
            #temp = re.compile("([0-9.E+]+)")
            
# =============================================================================
#             temp=re.compile(r'(\D+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)([\d.E+]+)(\s+)(\D+)')
#             
#             
#             
#             mystr=str(Lines[24])
#             
#             res=temp.match(mystr).groups()
# =============================================================================
            
# =============================================================================
#             res = temp.findall(mystr)
#             
#             res = [re.findall(r'(\D+)(\s+)([\d.E+]+)', mystr)[0] ] 
# =============================================================================
            
            #print('res = ', str(res))
            
# =============================================================================
#         data = pd.read_csv(
#             self.RP.fnameCSV,
#             header=7, #номер строки, где заканчиваются строки, занятые заголовком 
#             sep=' ',
#  
# # =============================================================================
# #             usecols=usecols,
# # 
# #             names=names
# # =============================================================================
# 
#             )
#         
#         Matrix=data.values
#         
#         print('data = ', Matrix)
# =============================================================================
            
            
        

a=ParserMFrac()

#a.SectionSplitter()
a.CutSectionByPatt()
a.ReadCSV()